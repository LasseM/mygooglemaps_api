import React from 'react';
import './App.css';
import GoogleMap from './googleMap'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>My Google Map App</h3>
      </header>
      
      <div className="mainSection">
        <GoogleMap/>
      </div>
        
    </div>
  );
}

export default App;
